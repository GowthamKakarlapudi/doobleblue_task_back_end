exports.routesConfig = function (app) {

  // app.get('/',function (req, res) {
  //   res.send('ROOT WEBSITE');
  // });
 
  app.get('/privacy*',function (req, res) {
    res.send('PRIVACY POLICY');
  });
  app.get('/*',function (req, res) {
    res.sendFile('/webui/index.html', { root: __dirname });
  });
 
  // app.get('/*',function (req, res) {
  //   res.send('SHOULD RESPOND SOMETHING');
  // });

  app.post('/stripe_payment', function (req, res) {
    res.sendFile('/webui/payment.html', { root: __dirname });
  });

}