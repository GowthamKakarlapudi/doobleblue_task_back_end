const mongoose = require('../../../common/services/mongoose.services').mongoose;

const Schema = mongoose.Schema;

const OwnerSchema = new Schema({
    hospital_id:Number,
    name:String
});

const DoctorsAvailabilitySchema = new Schema({
    doctor_id : String,
    doctor_name: String,
    hospital_id : Number,
    selected_date : Date,
    start_time : String,
    end_time : String
})

const BookingSchema = new Schema({
    patient_name:String,
    patient_age:Number,
    patient_temperature:Number,
    patient_mobile:Number,
    doctor_name:String,
    doctor_id:String,
    selected_slot:String,
    hospital_id:Number
})

const Owner = mongoose.model('Owner', OwnerSchema);
const DoctorsAvailability = mongoose.model('DoctorsAvailability',DoctorsAvailabilitySchema);
const Booking = mongoose.model('Bookings',BookingSchema);
OwnerSchema.findById = function (cb) {
    return this.model('Owner').find({id: this.id}, cb);
};
exports.getDoctors = () =>{
    return Owner.find();
}
exports.getDoctor = (data)=>{
    return Owner.find({hospital_id:data.hospital_id,name:data.name})
}
exports.addDoctor = (data) =>{
    const OwnerObj = new Owner(data);
    return OwnerObj.save();
}
exports.addDoctorAvailability = (data) =>{
    const DoctorObj = new DoctorsAvailability(data);
    return DoctorObj.save();
}
exports.checkDoctorAvailability = (data) =>{
    return DoctorsAvailability.find({doctor_id: data.doctor_id,selected_date:data.selected_date})
}
exports.getDoctorsAvailability = (data) =>{
    return DoctorsAvailability.find({hospital_id:data.hospital_id,selected_date:data.selected_date})
}
exports.makeSlotBooking = (data) =>{
    const BookingObj = new Booking(data);
    return BookingObj.save();
}
exports.getbookings = (id) =>{
    return Booking.find({hospital_id:id});
}

