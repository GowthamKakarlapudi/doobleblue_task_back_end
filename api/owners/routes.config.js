const OwnersController = require('./controllers/owners.controller');

exports.routesConfig = function (app) {

app.get('/get_list',[
  OwnersController.getOwners
]);
app.post('/add_Doctor', [
  OwnersController.addDoctor
]);
app.post('/add_doctor_availability',[
  OwnersController.addDoctorAvailability
]);
app.post('/get_doctor_availability',[
  OwnersController.getDoctorAvailability
]);
app.post('/make_slot_booking',[
  OwnersController.makeSlotBooking
]);
app.post('/get_bookings_by_hospital',[
  OwnersController.getBookings
]);


}