const OwnersModel = require ('../models/owners.model.js');
exports.getOwners = (req, res) =>{
    OwnersModel.getDoctors().then(data =>{
        res.status(200).send({status:200,result:data})
    })
}
exports.addDoctor = (req, res) =>{
    if(!req.body.name){
        res.status(400).send({status:400,message:"missing few parameters"})
    }
    OwnersModel.getDoctor(req.body).then(owners =>{
        if(!owners.length){

            OwnersModel.addDoctor(req.body).then(data => {
                res.status(200).send({status:200,message:"Added Doctor successfully",result:data})
            })
        }else res.status(200).send({status:200,message:"Doctor Alredy exists"})

    })
}
exports.addDoctorAvailability = (req, res) =>{
    OwnersModel.checkDoctorAvailability(req.body).then(data =>{
        if(!data.length){
            OwnersModel.addDoctorAvailability(req.body).then(data =>{
                res.status(200).send({status:200,message:"Added Doctor Availability Details",result:data})
            })
        }else res.status(200).send({status:200,message:"Docter With selected date already scheduled"})
    })
}
exports.getDoctorAvailability = async (req,res) =>{
    var staff_complete_details={
        doctor_details:[]
    }
    await OwnersModel.getDoctorsAvailability(req.body).then(data =>{
        staff_complete_details.doctor_details = data;
        res.status(200).send({status:200,result:staff_complete_details})  
    })
}
exports.makeSlotBooking =(req, res) =>{
    OwnersModel.makeSlotBooking(req.body).then(data=>{
        res.status(200).send({status:200,message:"added slot booking",result:data});
    })
}
exports.getBookings = (req,res) =>{
    OwnersModel.getbookings(req.body.hospital_id).then(data =>{
        res.status(200).send({status:200,result:data})
    })
}
